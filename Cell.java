public class Cell {

    private int openCloseStatus, index;
    Cell root;

    public Cell(int index)
    {
        this.openCloseStatus = 0; //as initial all closed
        this.index = index;
        this.root = null;
    }

    public int getOpenCloseStatus() {
        return openCloseStatus;
    }

    public void setOpenCloseStatus(int openCloseStatus) {
        this.openCloseStatus = openCloseStatus;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Cell getRoot() {
        return root;
    }

    public void setRoot(Cell root) {
        this.root = root;
    }
}
