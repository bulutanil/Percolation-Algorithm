# Percolation-Algorithm
Percolation Algorithm With Randomized Matrix


This is a solution for Coursera Algorithm Assignment 1.
I've used weighted quick union algorithm to solve percolation problem.

Steps:

1-) Generate random square matrix NXN (N is user input)\
2-) As initial all matrix cells are blocked\
3-) Generated random row and column number and opened that cell if it isn't already opened.\
4-) Union operation is made with neighbour cells.\
5-) Checking percolatation after each opened cell.\
