import java.util.HashMap;
import java.util.Random;

public class Percolation {

    private Cell[][] matrix;
    private HashMap<Cell, Integer> rootMap = new HashMap<Cell, Integer>();
    private int size;

    public Percolation(int n) {
        matrix = new Cell[n][n];
        size = n;

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                matrix[i][j] = new Cell((i * size + j));
                matrix[i][j].setRoot(matrix[i][j]);
                rootMap.put(matrix[i][j], 1);
            }
        }
    }

    public void startProgress()
    {
        boolean result = false;
        while(result != true)
        {
            int row = generateRandomNumber(0,size-1);
            int column = generateRandomNumber(0,size-1);

            if(!isOpen(row,column))
            {
                System.out.println("row:"+row+ " column:"+column);
                open(row,column);
                control(row,column);
                result = percolates();
            }

        }
        int numberOfOpenSites = numberOfOpenSites();
        System.out.println("open sites:"+numberOfOpenSites);
        System.out.println("probability =" +((double)numberOfOpenSites / (size * size)));
    }

    private void open(int row, int col)
    {
        matrix[row][col].setOpenCloseStatus(1);
    }

    private boolean isOpen(int row, int col)
    {
       return matrix[row][col].getOpenCloseStatus() == 1;
    }

    public int numberOfOpenSites()
    {
        int count = 0;

        for(int i = 0; i < size; i++)
        {
            for(int j = 0; j < size; j++)
            {
                if(isOpen(i,j))
                {
                    count++;
                }
            }
        }

        return count;
    }

    private boolean percolates()
    {
        for(int i = 0; i < 1 ; i++)
        {
            for(int j = 0; j < size; j++)
            {
                for(int k = 0; k < size; k++)
                {
                    if(isConnected(matrix[i][j],matrix[i+size-1][k])) //check is there a connection between top row and bottom row
                    {
                        return true;
                    }
                }

            }
        }
        return false;
    }

    private boolean isConnected(Cell p, Cell q)
    {
        return findRoot(p) == findRoot(q);
    }

    private int generateRandomNumber(int lower, int upper)
    {
        Random rand = new Random();
        return rand.nextInt((upper - lower) + 1) + lower;
    }

    private Cell findRoot(Cell p) {
        if (p.getIndex() == p.getRoot().getIndex()) {
            return p;
        }
        return findRoot(p.getRoot());
    }


    private void union(Cell p, Cell q) {
        Cell pRoot = findRoot(p);
        Cell qRoot = findRoot(q);

        if ( rootMap.get(pRoot) >=  rootMap.get(qRoot)) {
            rootMap.put(pRoot,  rootMap.get(pRoot) + 1);
            qRoot.setRoot(pRoot);
        } else {
            rootMap.put(qRoot, rootMap.get(qRoot) + 1);
            pRoot.setRoot(qRoot);
        }
    }

    private void control(int row, int col)
    {
        if (row == 0 && col == 0) //left upper corner cell
        {
            checkDown(row,col);
            checkRight(row,col);
        }
        else if (row == 0 && col == size - 1) //right upper corner cell
        {
            checkLeft(row,col);
            checkDown(row,col);
        }
        else if (row == size - 1 && col == 0) //left lower  corner cell
        {
            checkUp(row,col);
            checkRight(row,col);
        }
        else if (row == size - 1 && col == size - 1) //right lower corner cell
        {
            checkUp(row,col);
            checkLeft(row,col);
        }
        else //inside (not the 4 corner cells)
        {
            if (row == 0)
            {
                if (col != 0 && col != size - 1) //top row, but not the corner columns
                {
                    checkRight(row,col);
                    checkLeft(row,col);
                    checkDown(row,col);
                }
            }

            if (row == size - 1) {
                if (col != 0 && col != size - 1) //bottom row, but not the corner columns
                {
                    checkRight(row,col);
                    checkLeft(row,col);
                    checkUp(row,col);
                }
            }

            if (col == 0) {
                if (row != 0 && row != size - 1) //leftest column, but not the top and bottom rows
                {
                    checkRight(row,col);
                    checkDown(row,col);
                    checkUp(row,col);
                }
            }

            if (col == size - 1) {
                if (row != 0 && row != size - 1) //rightest column, but not the top and bottom rows
                {
                    checkLeft(row,col);
                    checkDown(row,col);
                    checkUp(row,col);
                }
            }

            if ((0 < row) && (row < size - 1) && (0 < col) && (col < size - 1))   //whole inside, not the 4 corners and edges
            {
                checkRight(row,col);
                checkLeft(row,col);
                checkDown(row,col);
                checkUp(row,col);
            }

        }
    }

    private void checkLeft(int row, int col)
    {
        if (isOpen(row,col - 1)) //near left
        {
            union(matrix[row][col], matrix[row][col - 1]);
        }
    }

    private void checkRight(int row, int col)
    {
        if (isOpen(row,col + 1)) //right cell
        {
            union(matrix[row][col], matrix[row][col + 1]);
        }
    }


    private void checkDown(int row, int col)
    {
        if (isOpen(row+1,col)) //down cell
        {
            union(matrix[row][col], matrix[row + 1][col]);
        }
    }

    private void checkUp(int row, int col)
    {
        if (isOpen(row-1,col))  //up cell
        {
            union(matrix[row][col], matrix[row - 1][col]);
        }
    }


}


